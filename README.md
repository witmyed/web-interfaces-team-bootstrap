# README #

ILW Web Interfaces - customised version of bootstrap

### What is this repository for? ###

Demonstrates a workflow for creating a customised version of Bootstrap

### How do I get set up? ###

* Download or clone repo

* Download the latest bootstrap source, http://getbootstrap.com/getting-started/#download, click on "Download source"

* Unzip 'bootstrap-VERSION#.zip' drop into repo folder and rename 'bootstrap-VERSION#' to 'bootstrap' 

* The directory /bootstrap/ is ignored by git (see .gitignore file) and is only used for LESS compilation (you will need a LESS compiler)

* Create a /css/ directory (also ignored git) 

* Compile /less/style.less to /css/style.css to. for example 'lessc /less/style.less /css/style.css'
